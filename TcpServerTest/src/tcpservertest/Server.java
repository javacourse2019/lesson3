
package tcpservertest;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Server
{
    private  Socket          clientSocket;    // Сокет клиента для получения клиентских потоков
    private  int             serverPort= 7777;
    
    private  ServerSocket     server;         // Серверный сокет для обслуживания клиентсикх сокетов
    private  InputStream      in;             // Поток чтения из сокета
    private  OutputStream     out;            // Поток записи в сокет  
    private static Scanner          sc;       
    
    
    public void start()
    {
        try
        {
            server = new ServerSocket(serverPort); // создание сервер
            System.out.println("Создан сервер");
            
            clientSocket = server.accept(); // Обработка подключения клиента и получение доступа 
            in = clientSocket.getInputStream();     // Получение доступа к потоку ввода клиентского сокета
            out = clientSocket.getOutputStream();   // Получение доступа к потоку вывода клиентского сокета
            
            if (in != null && out !=null)
            {
               System.out.println("Потоки открыты успешно"); 
               System.out.println("Подключен клиент");
               out.write("Подключено к серверу".getBytes());
               out.write("\n".getBytes());
               
            }
            else
            {
              System.out.println("Ошибка открытия потоков"); 
            }
                   
            
          
            
        } catch (IOException ex)
        {
            System.out.println("Ошибка запуска сервера");
        }
    
    }         
    
}
